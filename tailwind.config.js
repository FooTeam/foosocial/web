const blueGray = require('tailwindcss/colors').blueGray
const teal = require('tailwindcss/colors').teal
const sans = require('tailwindcss/defaultTheme').fontFamily.sans

module.exports = {
    purge: {
        enabled: true,
        content: ['./src/**/*.svelte', './src/**/.html'],
    },
    theme: {
        extend: {
            colors: {
                blueGray,
                teal,
            },
            fontFamily: {
                display: ['Poppins', ...sans],
            },
            gridTemplateRows: {
                layout: '1fr max-content',
                page: 'min-content min-content 1fr',
                auth: 'min-content 1fr min-content',
            },
        },
    },
}
