import { writable } from 'svelte/store'
import { localWritable } from './localStorage'

export const user = localWritable('user', undefined)

export const awaitingAPI = writable(false)

export const postId = writable(undefined)
