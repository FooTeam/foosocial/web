import { post } from './fetch'
import { user } from 'store'

/**
 * Handle the login
 * @param {string} username
 * @param {string} password
 *
 * @returns {Promise} The request promise
 */
export const login = async (username, password) =>
    post('login', { username, password }).then(({ req, res }) => {
        if (req.ok) {
            user.set(res)
            return
        }
    })

/**
 * Handle the registration
 * @param {string} username
 * @param {string} name
 * @param {string} password
 *
 * @returns {Promise} The request promise
 */
export const register = async (username, name, password) =>
    post('register', { username, name, password }).then(
        async ({ req, res }) => {
            if (req.ok && !res.errors) {
                await login(username, password)
                return
            }

            return res.errors
        }
    )
