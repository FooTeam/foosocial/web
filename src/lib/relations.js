import { localWritable } from '../localStorage'
import { get } from './fetch'
import { user } from 'store'

export const relations = localWritable('relations', [], async set => {
    const { req, res } = await get('api/relations')
    set(req.ok ? res.data : undefined)

    return () => {}
})

export const getRelationStatus = username => {
    return new Promise(resolve => {
        user.subscribe(u => {
            if (u.username === username) {
                resolve('yourself')
            }
        })

        relations.subscribe(r => {
            const relation = r.filter(
                relation => relation.user.username === username
            )
            resolve(relation.length ? relation[0].status : undefined)
        })
    })
}

const getRelation = username => {
    return new Promise(resolve => {
        relations.subscribe(relations => {
            const index = relations.findIndex(
                relation => relation.user.username === username
            )
            resolve({ ...relations[index], index })
        })
    })
}

const updateStatus = async (username, status) => {
    const relation = await getRelation(username)
    relations.update(r => {
        r[relation.index].status = status
        return r
    })
}

export const acceptFriend = username => {
    patch('api/relations', { username, accept: true })
    updateStatus(username, 'friend')
}

export const ignoreFriend = username => {
    patch('api/relations', { username, ignore: true })
    updateStatus(username, 'ignored')
}

export const requestFriend = async username => {
    const { req, res } = await post('api/relations', { username })
    if (!req.ok && res.error === 'twice') {
        relations.update(r => {
            r.push({ user: { username }, status: 'pending' })
            return r
        })
        return
    }

    relations.update(r => {
        r.push(res.data)
        return r
    })
}

export const deleteFriend = async username => {
    del('api/relations', { username })

    const relation = await getRelation(username)
    relations.update(r => {
        r.splice(relation.index, 1)
        return r
    })
}
