import { get } from './fetch'

function isUndefined(val) {
    return val === undefined
}

export function required(val) {
    return !isUndefined(val) && val !== null && val.length > 0 ? true : 'Requis'
}

export function min(val, args) {
    return !isUndefined(val) && val.length >= args[0]
        ? true
        : `Longeur minimale : ${args[0]}`
}

export function max(val, args) {
    return isUndefined(val) || val.length <= args[0]
        ? true
        : `Longueur maximale : ${args[0]}`
}

export function between(val, args) {
    let result = min(val, [args[0]])
    if (result !== true) return result

    result = max(val, [args[1]])

    return result
}

export function alpha_dash(val) {
    return /^[a-z0-9_-]+$/.test(val)
        ? true
        : 'Ne peut contenir que des lettres minuscules, des chiffres, des tirets (-) et des underscores (_)'
}

export function confirmation(val, args) {
    if (val === args[0] && required(val)) {
        return true
    }

    return 'Les mots de passe ne correspondent pas'
}

export async function unique(val) {
    const { res } = await get('validate/username-unique', { username: val })

    if (res?.errors?.username.includes('unique')) {
        return `Le nom d'utilisateur ${val} n'est pas disponible`
    }

    return true
}
