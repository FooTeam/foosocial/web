import * as rules from './rules'

export default async inputs => {
    return Object.values(inputs).reduce(
        async (validated, input) =>
            (await validated) && (await input.validate()),
        true
    )
}

export const validateSelf = async (inputRules, value) => {
    let errors = []

    for (const rule of inputRules) {
        const [name, ...args] = rule.split(/:/g)
        const validated = await rules[name].call(null, value, args)
        if (validated !== true) {
            errors.push(validated)
        }
    }

    return errors
}
