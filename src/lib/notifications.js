import { localWritable } from '../localStorage'
import { get } from 'lib/fetch'

export const notifications = localWritable(
    'notifications',
    { requests: [], answers: [] },
    set => {
        get('api/notifications').then(({ res, req }) => {
            if (req.ok) {
                res.answers.forEach(answer => {
                    answer.read = false
                })
                set(res)
            }
        })

        return () => {}
    }
)
