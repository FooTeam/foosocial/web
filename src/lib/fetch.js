import { awaitingAPI, user } from 'store'
import { get as storeGet } from 'svelte/store'
import { getXsrfToken } from './xsrf'

export const get = (input, data = undefined) =>
    req(input + (data ? '?' + new URLSearchParams(data) : ''))

export const post = (input, body) => req(input, 'POST', { body })

export const patch = (input, body) => req(input, 'PATCH', { body })

export const del = (input, body) => req(input, 'DELETE', { body })

/**
 *
 * @param {string} input Request endpoint
 * @param {string} method Request HTTP method
 * @param {object} init Additionnal settings to pass to the request
 *
 * @returns {{req: Request, res: object}} The request object and the result of the executed request
 */
const req = async (input, method = 'GET', init = {}) => {
    awaitingAPI.set(true) // We are fetching the API
    const req = await fetch(`${process.env.API_URL}/${sanitizeInput(input)}`, {
        method,
        credentials: 'include',
        headers: {
            'Content-type': 'application/json',
            Accept: 'application/json',
            'X-XSRF-TOKEN': await getXsrfToken(),
        },
        ...init,
        body: JSON.stringify(init.body),
    })
    awaitingAPI.set(false) // We finished fetching the API

    if (storeGet(user) !== undefined && req.status == 401) {
        user.set(undefined)
        return { req }
    }

    return { req, res: await req.json() }
}

/**
 * Trim leading slashes in an input string
 * @param {string} input Request endpoint
 *
 * @returns {string} The sanitized input
 */
const sanitizeInput = input => {
    return input.replace(/^\/+/, '')
}
