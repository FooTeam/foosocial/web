/**
 * Send a request to refresh the XSRF-TOKEN Cookie
 */
export const xsrfTokenRefresh = async () => {
    await fetch(`${process.env.API_URL}/sanctum/csrf-cookie`, {
        credentials: 'include',
    })
}

/**
 * Parse the XSRF-TOKEN Cookie to retrieve its value
 *
 * @returns {string} The value of the XSRF-TOKEN Cookie
 */
export const getXsrfToken = async () => {
    await xsrfTokenRefresh()

    for (const cookieString of document.cookie.split(';')) {
        const cookie = cookieString.split('=')
        if (cookie[0].includes('XSRF-TOKEN')) {
            return decodeURIComponent(cookie[1])
        }
    }
}
