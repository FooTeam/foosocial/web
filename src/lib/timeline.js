import { get } from './fetch'
import { get as getStore } from 'svelte/store'
import { localWritable } from '../localStorage'

export const timeline = localWritable('timeline', [])

export const refresh = async () => {
    const { req, res } = await get('api/posts')
    if (!req.ok) {
        return null
    }

    timeline.set(res.data)
    return res.next
}

export const loadNext = async next => {
    const { req, res } = await get('api/posts', { page: next })
    if (!req.ok) {
        return null
    }

    timeline.set([...getStore(timeline), ...res.data])
    return res.next
}
