import { writable, get } from 'svelte/store'

export function localWritable(key, initialValue, setter) {
    const store = writable(initialValue, setter)
    const { subscribe, set } = store

    if (process.browser) {
        const json = localStorage.getItem(key)

        if (json) {
            set(JSON.parse(json))
        }
    }

    return {
        set(value) {
            if (process.browser) {
                localStorage.setItem(key, JSON.stringify(value))
            }
            set(value)
        },
        update(callback) {
            const value = callback(get(store))
            this.set(value)
        },
        subscribe,
    }
}
